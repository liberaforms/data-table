/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

//import i18n from '@/i18n';
//const $t = i18n.global.t

//import { parseISO, format } from 'date-fns'
import _ from 'underscore';

export function orderItems(items, field_name, ascending) {
  if (field_name == 'marked' || field_name == 'created') {
    items = _.sortBy(items, field_name)
  } else {
    items.sort(function compareFn(first_el, second_el) {
      var first_data;
      var second_data;
      if (field_name.endsWith('__html')) {
        first_data = first_el.data[field_name].value
        second_data = second_el.data[field_name].value
      } else {
        first_data = first_el.data[field_name]
        second_data = second_el.data[field_name]
      }
      if (first_data === undefined || !first_data) {
        return -1;
      }
      if (second_data === undefined) {
        return 1;
      }
      if (isNaN(first_data)) {
        first_data = first_data.toLowerCase()
        second_data = second_data.toLowerCase()
      } else {
        first_data = parseInt(first_data)
        second_data = parseInt(second_data)
      }
      if (first_data < second_data) {
        return -1;
      }
      return 1;
    })
  }
  return ascending == true ? items : items.reverse()
}

export function isMultiChoice(field_name) {
  if (!field_name) {
    return false
  }
  if (field_name.startsWith('checkbox') ||
      field_name.startsWith('radio-group') ||
      field_name.startsWith('select')){
    return true
  }
  return false
}

export function isNumeric(field) {
  // should rename this function because it feels we are testing against a number
  return !field.name ? false : field.name.startsWith('number')
}

export function isFloat(n) {
    if (typeof n !== 'number') {
        return false;
    }
    return Number(n) === n && n % 1 !== 0;
}
