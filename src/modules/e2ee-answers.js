/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

const openpgp = require('openpgp');
//
// Data conversion code
//
function isString(data)
{
    return (typeof data === 'string' || data instanceof String);
}
function binaryStringToBytes(bins)
{
    const bytes = new Uint8Array(bins.length);
    for (var i = 0; i < bytes.length; ++i)
        bytes[i] = bins.charCodeAt(i);
    return bytes;
}
export const b64 = {
    to: function(bytes) {
        return btoa(String.fromCharCode.apply(null, bytes));
    },
    from: function(b64string) {
        const bins = atob(b64string);
        return binaryStringToBytes(bins);
    },
    ensureBinary: function(data) {
        const binaryData = isString(data) ? b64.from(data) : data;
        return binaryData;
    },
    ensureString: function(data) {
        const strData = isString(data) ? data : b64.to(data);
        return strData;
    }
}
export const stream = {
    toBlob: async function(stream) {
        // We don't have an easy way to convert streams to a blob
        // So we read the whole stream (probs. ciphered file) in memory
        const streamReader = stream.getReader();
        const streamData = [];
        try {  // Loop because async loops are not supported on safari as of 2024-08-01
          while (true) {
            const { done, value } = await streamReader.read();
            if (done) break;
            if (isString(value))
            {
              streamData.push(binaryStringToBytes(value));
            }
            else
              streamData.push(value.map((i) => (isString(i)) ? binaryStringToBytes(i) : i));
          }
        } finally {
          streamReader.releaseLock();
        }
        return new Blob(streamData);
    }
}
//
// PrivateKey storage-related code
//
export const keyStorage = {
    genKeyUserId: function(keyType, identifier) {
        // We'll get something like:
        // 1@form.keys.usem.liberaforms.org
        // 3@editor.keys.usem.liberaforms.org
        return `${identifier}@${keyType}.keys.${window.location.hostname}`
    },
    getStorageId: function getStorageIdForKey(keyId) {
        return `LF_E2EE_Key_${keyId}`;
    },
    persistRawKey: function(keyId, rawKey, keyProtection, useSessionStorage, publicKey) {
        const keyStorageId = keyStorage.getStorageId(keyId);
        const storage = useSessionStorage ? sessionStorage : localStorage;
        // Persist key
        storage.setItem(keyStorageId,
            b64.ensureString(rawKey));
        if (publicKey)
            storage.setItem(`${keyStorageId}_public`, publicKey)
        else
            storage.removeItem(`${keyStorageId}_public`)
        // And lock type
        if (keyProtection)
            storage.setItem(`${keyStorageId}_lock`,
                isString(keyProtection) ? "passphrase" : "userKey");
        else
            storage.removeItem(`${keyStorageId}_lock`);
    },
    readLocked: function(keyId) {
        const storageId = this.getStorageId(keyId);
        return localStorage.getItem(storageId);
    },
    readPublic: function(keyId, useSessionStorage=false) {
        const storageId = this.getStorageId(keyId);
        if (useSessionStorage) {
          return sessionStorage.getItem(`${storageId}_public`);
        }
        return localStorage.getItem(`${storageId}_public`)
        // Keys protected by userKey cannot be directly accessed, we read _public instead
        //if (localStorage.getItem(`${storageId}_lock`) === "userKey")
        //{
        //    return localStorage.getItem(`${storageId}_public`)
  	    //}
        //return this.readLocked(keyId);
    },
    E_NEED_PASSPHRASE: 'E_NEED_PASSPHRASE',
    readUnlocked: async function(keyId, userKeyId, keyPassphrase) {
        const storageId = this.getStorageId(keyId);
        let unlockedKey;
        if (sessionStorage.hasOwnProperty(storageId)) {
            // First try to read session storage
            unlockedKey = sessionStorage.getItem(storageId)
        }
        if (!unlockedKey) {
            // Then try to read local storage
            const lockedKey = localStorage.getItem(storageId)
            let lockKind = localStorage.getItem(`${storageId}_lock`)
            if (lockKind === null && lockedKey) {
                const privKey = await keys.getPrivate(lockedKey, keyPassphrase);
                if (!privKey.isDecrypted()) {
                    localStorage.setItem(`${storageId}_lock`, 'passphrase')
                    lockKind = 'passphrase'
                }
            }
            if (!lockedKey) {return null}
            if (lockKind === 'passphrase') {
               try {
                   const privKey = await keys.getPrivate(lockedKey, keyPassphrase);
                   return (privKey.isDecrypted()) ? privKey : this.E_NEED_PASSPHRASE;
               }
               catch {
                   return this.E_NEED_PASSPHRASE;
               }
            }
            else if (lockKind === 'userKey' && userKeyId) {
                const userKey = await this.readUnlocked(userKeyId, null, keyPassphrase);
                if (userKey == this.E_NEED_PASSPHRASE) {
                  return this.E_NEED_PASSPHRASE;
                }
                if (userKey)
                    unlockedKey = await userKey.decryptBinary(lockedKey);
            }
            else
                unlockedKey = lockedKey;
        }
        if (unlockedKey)
            return await keys.getPrivate(unlockedKey);
        return null;
    },
}
//
// Actual cryptography-related code
//
export const keys = {
    getPublic: async function(publicKey) {
        if (!publicKey) return null;
        const binaryPublicKey = b64.ensureBinary(publicKey);
        var pgpKey = await openpgp.readKey({
            binaryKey: binaryPublicKey
        });
        return new LF_PublicKey(pgpKey)
    },
    getPrivate: async function(privateKey, keyPassphrase) {
        if (!privateKey) return null;
        const binaryPrivateKey = b64.ensureBinary(privateKey);
        let pgpKey = await openpgp.readPrivateKey({
            binaryKey: binaryPrivateKey
        })
        if (!pgpKey.isDecrypted() && keyPassphrase) {
            // Get unlocked/decrypted key
            pgpKey = await openpgp.decryptKey({
                privateKey: pgpKey,
                passphrase: keyPassphrase,
            });
        }
        return new LF_PrivateKey(pgpKey);
    },
    generate: async function(userKeyId, passphrase='') {
        const {
            privateKey
        } = await openpgp.generateKey({
            type: 'ecc',
            userIDs: [{
                name: userKeyId
            }],
            format: 'binary',
            passphrase: passphrase, // encrypt key with passphrase
        });
        return this.getPrivate(privateKey); // ,passphrase) return decrypted
    },
    isKey: function(key) {
      try {
        return '_pgpKey' in key
      }
      catch {
        return false
      }
    },
}
//
// Helper local functions for more domain-specific cryptographic key objects
//
async function userKeyIdFromOpenPGPKey(pgpKey) {
    const user = await pgpKey.getPrimaryUser();
    return user.user.userID.userID;
}
class LF_Key {
    constructor(pgpKey) {
        this._pgpKey = pgpKey;
    }
    async userKeyId() {
        return await userKeyIdFromOpenPGPKey(this._pgpKey)
    }
    get fingerprint() {
        return this._pgpKey.getFingerprint().toUpperCase();
    }
}
class LF_PublicKey extends LF_Key {
    get publicKey() {
        return this._pgpKey;
    }
    export () {
        return b64.ensureString(this._pgpKey.write());
    }
    async encrypt(data) {
        const binaryCipher = await openpgp.encrypt({
            encryptionKeys: this._pgpKey,
            message: await openpgp.createMessage({
                text: data
            }),
            format: 'binary',
        });
        return b64.ensureString(binaryCipher);
    }
    async encryptBinary(data) {
        const binaryCipher = await openpgp.encrypt({
            encryptionKeys: this._pgpKey,
            message: await openpgp.createMessage({
                binary: data
            }),
            format: 'binary',
        });
        return binaryCipher;
    }
}
class LF_PrivateKey extends LF_Key {
    constructor(pgpKey) {
        super(pgpKey);
        if (pgpKey.isPrivate() && pgpKey.isDecrypted())
        {
            // Save in session storage
            this.persist(null, true);
        }
    }
    get privateKey() {
        return this._pgpKey;
    }
    toPublic() {
        return new LF_PublicKey(this._pgpKey.toPublic());
    }
    async decryptKey(keyProtection) {
      await openpgp.decryptKey({
          privateKey: this._pgpKey,
          passphrase: keyProtection,
      })
    }
    async export (keyProtection) {
        let encryptedKey;
        if (keyProtection) {
            if (isString(keyProtection)) { // Passphrase-protected
                encryptedKey = (await openpgp.encryptKey({
                    privateKey: this._pgpKey,
                    passphrase: keyProtection,
                })).write()
            }
            else {  // Assume public Key
                encryptedKey = await keyProtection.encryptBinary(
                    this._pgpKey.write())
            }
        }
        else
            // Unprotected export
            encryptedKey = this._pgpKey.write();
        // Return binary data
        return encryptedKey;
    }
    async decrypt(encData, format) {
        const binaryData = b64.ensureBinary(encData);
        const {
            data: decrypted
        } = await openpgp.decrypt({
            decryptionKeys: this._pgpKey,
            message: await openpgp.readMessage({
                binaryMessage: binaryData
            }),
            format: format || 'utf8',
        });
        return decrypted;
    }
    isDecrypted() { return this._pgpKey.isDecrypted() }
    async decryptBinary(encData) {
        return await this.decrypt(encData, 'binary');
    }
    async persist(keyProtection, useSessionStorage) {
        await keyStorage.persistRawKey(
            await this.userKeyId(),
            await this.export(keyProtection),
            keyProtection,
            useSessionStorage,
            this.toPublic().export())
    }
}

export async function deleteDeviceKeys(key_id="") {
  let key_prefix = key_id ? `LF_E2EE_Key_${key_id}` : 'LF_E2EE_Key_'
  var key_names = []
  async function deleteKeys() {
    function addKey(name) {
      if (!key_names.includes(name))
        key_names.push(name);
    }
    for (let i = 0; i < localStorage.length; i++) {
      let key = await localStorage.key(i)
      if (key.startsWith(key_prefix))
        addKey(key)
    }
    key_names.forEach((key) => localStorage.removeItem(key))
    key_names = []
    for (let i = 0; i < sessionStorage.length; i++) {
      let key = await sessionStorage.key(i)
      if (key.startsWith(key_prefix))
        addKey(key)
    }
    key_names.forEach((key) => sessionStorage.removeItem(key))
  }
  await deleteKeys()
}
