
import { isFloat } from '@/modules/items.js'


export function centerPoint2coords (centerPoint) {
  function is_valid_coord(value) {
    value = value.trim()
    if (value!=="") {
      let n = Number(value)
      if ( Number.isInteger(n) || isFloat(n) ) {
        return true
      }
    }
    return false
  }
  try {
    let coords = centerPoint.trim().split(',')
    if (coords.length == 2 &&
        is_valid_coord(coords[0]) &&
        is_valid_coord(coords[1])) {
      return coords
    }
  }
  catch {
    return []
  }
  return []
}
