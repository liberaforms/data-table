/*
This file is part of LiberaForms

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import i18n from '@/i18n';
import { Vue3Mq } from "vue3-mq";
import SlideUpDown from 'vue-slide-up-down'
import DataDisplay from "@/components/DataDisplay.vue";

document.querySelectorAll("[vue-component=data-display]")
        .forEach((element) => {
            createApp(DataDisplay).use(i18n)
                                  .use(createPinia())
                                  .use(Vue3Mq, {
                                    // config options here
                                  })
                                  .component("slide-up-down", SlideUpDown)
                                  .provide('endpoint', element.dataset.endpoint)
                                  .provide('csrf_token', element.dataset.csrf_token)
                                  .provide('is_e2ee', element.dataset.is_e2ee)
                                  .provide('e2ee_form_key_id', element.dataset.e2ee_form_key_id)
                                  .provide('e2ee_editor_key_id', element.dataset.e2ee_editor_key_id)
                                  .provide('e2ee_status', element.dataset.e2ee_status)
                                  // user key
                                  .provide('public_key_on_server', element.dataset.e2ee_public_key_on_server)
                                  .provide('ui_language', element.dataset.ui_language)
                                  .provide('display_as', element.dataset.display_as)
                                  .provide('enable_exports', element.dataset.enable_exports)
                                  .mount(element)
});
