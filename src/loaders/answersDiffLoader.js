/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import i18n from '@/i18n';
import { Vue3Mq } from "vue3-mq";
import AnswersDiff from "@/components/AnswersDiff.vue";

document.querySelectorAll("[vue-component=answers-diff-renderer]")
        .forEach((element) => {
            createApp(AnswersDiff).use(i18n)
                                  .use(createPinia())
                                  .use(Vue3Mq, {
                                    // config options here
                                  })
                                  .provide('ui_language', element.dataset.ui_language)
                                  .provide('endpoint', element.dataset.endpoint)
                                  .provide('is_e2ee', element.dataset.is_e2ee)
                                  .provide('e2ee_status', element.dataset.e2ee_status)
                                  .provide('public_key_on_server', element.dataset.e2ee_public_key_on_server) // user key
                                  .provide('e2ee_editor_key_id', element.dataset.e2ee_editor_key_id)
                                  .provide('e2ee_form_key_id', element.dataset.e2ee_form_key_id)
                                  .mount(element)
});
