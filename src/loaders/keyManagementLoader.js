/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import { createApp } from 'vue'
import i18n from '@/i18n';
import { Vue3Mq } from "vue3-mq";
import KeyManager from "@/components/KeyManager.vue";

document.querySelectorAll("[vue-component=key-manager-renderer]")
        .forEach((element) => {
            createApp(KeyManager).use(i18n)
                                 .use(Vue3Mq, {
                                   // config options here
                                 })
                                 .provide('csrf_token', element.dataset.csrf_token)
                                 .provide('e2ee_form_key_id', element.dataset.e2ee_form_key_id)
                                 .provide('e2ee_editor_key_id', element.dataset.e2ee_editor_key_id)
                                 .provide('e2ee_status', element.dataset.e2ee_status)
                                 .provide('endpoint', element.dataset.endpoint)
                                 .provide('ui_language', element.dataset.ui_language)
                                 .provide('public_key_on_server', element.dataset.e2ee_public_key_on_server) // user key
                                 .provide('external_restore', element.dataset.external_restore)
                                 .mount(element)
});
